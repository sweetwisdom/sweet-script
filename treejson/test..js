const jsonTree = require("./el-treejson");
JSON;

//模拟的json数据
let data = [
  { id: 1, parent: 0, level: 0, lable: "重庆" },
  { id: 11, parent: 1, level: 1, lable: "上海" },
  { id: 12, parent: 1, level: 1, lable: "北京" },
  { id: 121, parent: 12, level: 1, lable: "合肥" },
  { id: 2, parent: 0, level: 1, lable: "天津" },
  { id: 21, parent: 2, level: 1, lable: "丰田" },
  { id: 211, parent: 21, level: 1, lable: "饭店" },
  { id: 3, parent: 0, level: 0, lable: "厄尔" },
  { id: 4, parent: 0, level: 1, lable: "汇报" },
  { id: 31, parent: 3, level: 1, lable: "宝宝" },
  { id: 32, parent: 3, level: 1, lable: "茁壮" }
];

let rule = {
  id: "id",
  pid: "parent",
  children: "children"
};
let aa = jsonTree(data, rule);
console.log(JSON.stringify(aa, null, "\t"));
