//将后台的json转成jsonTree格式
// data 要整理的数据  rule 排序规则

var arrysort = function(arr, rule) {
  var ids = rule.id || "id",
    pids = rule.pid || "pid",
    dates = rule.date || "date";
  //   console.log(ids, pids, dates);
  var idmap = {},
    arraysort = [];
  arr.forEach(al => {
    // console.log(al);
    // console.log(al[ids]);
    if (!idmap[al[ids]]) {
      arraysort.push({
        [ids]: al[ids], //id  依赖字段 可自行更改
        data: [al]
      });

      idmap[al[ids]] = al;
      //   console.log(idmap);
    } else {
      for (var j = 0; j < arraysort.length; j++) {
        var dj = arraysort[j];
        if (dj.id == al[ids]) {
          //id 依赖字段 可自行更改
          dj.data.push(al);
          break;
        }
      }
    }
  });
  return arraysort;
};
var arr = [
  { id: "1001", name: "值1-1", value: "111" },
  { id: "1001", name: "值1-2", value: "11111" },
  { id: "1002", name: "值2-3", value: "25462" },
  { id: "1002", name: "值2-4", value: "23131" },
  { id: "1002", name: "值2-5", value: "2315432" },
  { id: "1001", name: "值3-1", value: "333333" }
];
let rule = {
  id: "name",
  pid: "parent",
  date: "date"
};
let aa = arrysort(arr, rule);
console.log(JSON.stringify(aa, null, "\t"));
module.exports = arrysort;
