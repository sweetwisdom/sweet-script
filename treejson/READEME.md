# TPREEJSON使用说明

# 前言：

最近用vue 做后台管理项目，做文章目录这一块的时候，需要Mysql 返回树形数据，很是绞尽脑汁。

在网上找到了大神的方法，稍微理解了一下，效果如下：

![1583738797887](assert/READEME/1583738797887.png)

# 适用于对象

Elemet-ui , antdesign 等树形控件的数据对接



# 使用方法

```js
//将后台的json转成jsonTree格式
// data 要整理的数据 rule 排序规则
// json(data,rule)
const jsonTree = require('./el-treejson')

let data = []// 数组

let rule =  rule ={
    id: "id",
    pid: "parent",
    children: "children"
  } // id来自于data 中的数据


```



> 更多详细请参照：test.js



