//将后台的json转成jsonTree格式
// data 要整理的数据  rule 排序规则

var jsonTree = function(data, rule) {
    
    var id = rule.id || "id",
      pid = rule.pid || "pid",
      children = rule.children || "children";
    var idMap = [],
      jsonTree = [];
    data.forEach(function(v) {
      idMap[v[id]] = v;
    });
    data.forEach(function(v) {
      var parent = idMap[v[pid]];
      delete v.parent; 
      if (parent) {
        !parent[children] && (parent[children] = []);
        parent[children].push(v);
      } else {
        jsonTree.push(v);
      }
    });
    return jsonTree;
  };
  module.exports= jsonTree