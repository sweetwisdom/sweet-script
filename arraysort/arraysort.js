//  功能:实现数组分类,去除多余数据
var arrySort = function(arr, rule) {
  var ids = rule.id || "id",
    names = rule.name || "name",
    dates = rule.date || "date";
  childId = rule.childname || "conid";
  //   console.log(ids, pids, dates);
  var idmap = {},
    arraysort = [];
  arr.forEach(al => {
    if (!idmap[al[ids]]) {
      if (al[[childId]]) {
        arraysort.push({
          [ids]: al[ids],
          [names]: al[names],
          value: "",
          [dates]: [al]
        });
      } else {
        arraysort.push({
          [ids]: al[ids],
          [names]: al[names],
          value: "",
          [dates]: null
        });
      }

      idmap[al[ids]] = al;
      //   console.log(idmap);
    } else {
      for (var j = 0; j < arraysort.length; j++) {
        var dj = arraysort[j];
        // console.log(al.conid);

        if (dj[[ids]] == al[ids] && al.conid) {
          //id 依赖字段 可自行更改
          dj[[dates]].push(al);
          break;
        }
      }
    }
  });
  return arraysort;
};
// var arr = [
//   { id: "1001", name: "值1-1", value: "111" },
//   { id: "1001", name: "值1-1", value: "11111" },
//   { id: "1002", name: "值2-3", value: "25462" },
//   { id: "1002", name: "值2-4", value: "23131" },
//   { id: "1002", name: "值2-5", value: "2315432" },
//   { id: "1001", name: "值3-1", value: "333333" }
// ];
// let rule = {
//   id: "name",
//   name: "name",
//   date: "date666"
// };
// let aa = arrysort(arr, rule);
// console.log(JSON.stringify(aa, null, "\t"));
module.exports = arrySort;
