const arraySort = require("./arraysort");
var arr = [
  { cid: 1, cname: "车型介绍", conid: 1, name: "2020款XX电动款" },
  { cid: 1, cname: "车型介绍", conid: 8, name: "2020款车型介绍" },
  { cid: 2, cname: "新车准备", conid: 6, name: "2020款新款车准备" },
  { cid: 3, cname: "保养", conid: 5, name: "2020款XX3通用保养" },
  { cid: 3, cname: "保养", conid: 7, name: "AX7 保养须知" },
  { cid: 4, cname: "发动机", conid: null, name: null },
  { cid: 5, cname: "离合器", conid: 2, name: "2020款XX发动机" },
  { cid: 6, cname: "变速器", conid: null, name: null },
  { cid: 7, cname: "车桥和悬架", conid: null, name: null },
  { cid: 8, cname: "制动", conid: null, name: null },
  { cid: 9, cname: "转向", conid: null, name: null },
  { cid: 10, cname: "约束系统", conid: null, name: null },
  { cid: 11, cname: "车身", conid: null, name: null },
  { cid: 12, cname: "空调", conid: null, name: null },
  { cid: 13, cname: "电气系统", conid: null, name: null },
  { cid: 14, cname: "动力总成", conid: null, name: null },
  { cid: 15, cname: "储能供电", conid: null, name: null },
  { cid: 16, cname: "附录", conid: null, name: null }
];

let rule = {
  id: "cid",
  name: "cname",
  date: "data",
  childname: "conid"
};
let aa = arraySort(arr, rule);
//  console.log();
console.log(JSON.stringify(aa, null, "\t"));
